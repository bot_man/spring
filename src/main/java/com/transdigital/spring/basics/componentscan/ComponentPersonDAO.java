package com.transdigital.spring.basics.componentscan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.transdigital.spring.basics.sping_in_5_stages.scope.JdbcConnection;

@Component
public class ComponentPersonDAO {
	@Autowired
	ComponentPersonDAO jdbcConnection;

	public ComponentPersonDAO getJdbcConnection() {
		return jdbcConnection;
	}

	public void setJdbcConnection(ComponentPersonDAO jdbcConnection) {
		this.jdbcConnection = jdbcConnection;
	}
	
}
