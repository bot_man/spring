package com.transdigital.spring.basics.sping_in_5_stages;

//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.transdigital.spring.basics.sping_in_5_stages.basic.BinarySearchImpl;

//@SpringBootApplication
@Configuration
@ComponentScan
public class SpingIn5StagesBasicApplication {

	public static void main(String[] args) {
	//	BinarySearchImpl binarySearch=new BinarySearchImpl(new BubbleSortAlgorithm());
		//int result= binarySearch.binarySearch(new int[] {1,3,5,7,2,6}, 3);
		//System.out.println(result);
		
		//springapplicationcontext
//		//ConfigurableApplicationContext applicationcontext=SpringApplication.run(SpingIn5StagesBasicApplication.class, args);
	try(AnnotationConfigApplicationContext applicationcontext = new AnnotationConfigApplicationContext(SpingIn5StagesBasicApplication.class)){
		BinarySearchImpl binarySearch=applicationcontext.getBean(BinarySearchImpl.class);
		//for prototype bean binarysearch and binarysearch1 values different
		//
		BinarySearchImpl binarySearch1=applicationcontext.getBean(BinarySearchImpl.class);
		
		
		//System.out.println(binarySearch1);
		//System.out.println(binarySearch);
		
		int result =binarySearch.binarySearch(new int[] {2,3,6,4}, 3);
		System.out.println(result);
		//applicationcontext.c
	}
	
	}

}
