package com.transdigital.spring.basics.sping_in_5_stages;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import com.transdigital.spring.basics.componentscan.ComponentPersonDAO;
import com.transdigital.spring.basics.sping_in_5_stages.basic.BinarySearchImpl;
import com.transdigital.spring.basics.sping_in_5_stages.scope.PersonDAO;


//@SpringBootApplication
@Configuration
//@ComponentScan
@ComponentScan("com.transdigital.spring.basics.componentscan")
public class SpingIn5StagesComponentScanApplication {

	
	//this program fails cause cause componentscan doesnt pick up by default the components of packages not part of this default package
	// in this scenario weve to add @ComponentScanto our springboot application
	//logger
	private static Logger LOGGER = LoggerFactory.getLogger(SpingIn5StagesComponentScanApplication.class);
	
	
	
	public static void main(String[] args) {
	
		
		
		
		
		
		
		//springapplicationcontext
		try(AnnotationConfigApplicationContext applicationcontext=new AnnotationConfigApplicationContext(SpingIn5StagesComponentScanApplication.class)){
		ComponentPersonDAO componentDAO=applicationcontext.getBean(ComponentPersonDAO.class);

		
		LOGGER.info("{}", componentDAO);
		 
		}		
		

	}

}
