package com.transdigital.spring.basics.sping_in_5_stages;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class SomeExternalServices {
	
	//app.properties--external.service.url
	
	@Value("${external.service.url}")
	private String url;

	public String returnserviceURL() {
		
		return url;
		
	}

}
