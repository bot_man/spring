package com.transdigital.spring.basics.sping_in_5_stages;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import com.transdigital.spring.basics.sping_in_5_stages.basic.BinarySearchImpl;
import com.transdigital.spring.basics.sping_in_5_stages.cdi.SomeCdiBusiness;
import com.transdigital.spring.basics.sping_in_5_stages.scope.PersonDAO;


//@SpringBootApplication
@Configuration
@ComponentScan
public class SpingIn5StagesCdiApplication {

	//logger
	private static Logger LOGGER = LoggerFactory.getLogger(SpingIn5StagesCdiApplication.class);
	
	
	
	public static void main(String[] args) {
	
		
		
		
		
		
		
		//springapplicationcontext
		try(AnnotationConfigApplicationContext applicationcontext=new AnnotationConfigApplicationContext(SpingIn5StagesCdiApplication.class)){
		SomeCdiBusiness business=applicationcontext.getBean(SomeCdiBusiness.class);

		//busisness bean printed
		
		LOGGER.info("{} dao--{}", business,business.getSomeCdiDao());
		

		
		}
	}

}
