package com.transdigital.spring.basics.sping_in_5_stages.basic;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

//bydefault every bean is singleton
//for prototype bean do this
//@Scope("prototype")
//can also be denoted as
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class BinarySearchImplPostConstructPreDestroy {
	
	//logggger
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	//using interface
	@Autowired
	//private SortAlgorithm sortAlgorithm;
	//using constuctor
	//public BinarySearchImpl(SortAlgorithm sortAlgorithm) {
		//super();
		//this.sortAlgorithm=sortAlgorithm;
		
//	}
	
	//using setter injection
	//rtclick+source+getters&setters
	//public void setSortAlgorithm(SortAlgorithm sortAlgorithm) {
		//this.sortAlgorithm = sortAlgorithm;
	//}

	//using no constructor
	//private SortAlgorithm sortAlgorithm;
	
	//autowiringbyname
	
	//private SortAlgorithm bubbleSortAlgorithm;
	
	//or
	
	//private SortAlgorithm quickSortAlgorithm;
	
	
	//method three using @qualifier
	@Qualifier("bubble")
	private SortAlgorithm sortAlgorithm;
	
	public int binarySearch(int[] numbers, int numberToSearchFor) {
		
		// BubbleSortAlgorithm bubbleSort= new BubbleSortAlgorithm();
//		 int [] sortedNumbers=bubbleSort.sort(numbers);
		 int [] sortedNumbers=sortAlgorithm.sort(numbers);
		 System.out.println(sortAlgorithm);
		//sort the array
		//search the array
		//return the index 
		
		return numberToSearchFor;
	}
	
	@PostConstruct
	public void postConstruct() {
		logger.info("postconstruct called");
	}

	@PreDestroy
	public void preDestroy() {
		logger.info("predestroy called");
	}


}
