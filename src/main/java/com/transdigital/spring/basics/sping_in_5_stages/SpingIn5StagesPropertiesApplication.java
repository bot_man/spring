package com.transdigital.spring.basics.sping_in_5_stages;

//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import com.transdigital.spring.basics.sping_in_5_stages.basic.BinarySearchImpl;

//@SpringBootApplication
@Configuration
@ComponentScan
@PropertySource("classpath:app.properties")
//app.properties
public class SpingIn5StagesPropertiesApplication {

	public static void main(String[] args) {
	try(AnnotationConfigApplicationContext applicationcontext = new AnnotationConfigApplicationContext(SpingIn5StagesPropertiesApplication.class)){
		
		SomeExternalServices service=applicationcontext.getBean(SomeExternalServices.class);
		
		
		System.out.println(service.returnserviceURL());
		
	}
	
	}

}
