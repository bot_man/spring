package com.transdigital.spring.basics.sping_in_5_stages;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
//import org.springframework.boot.SpringApplication;
//import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;

import com.transdigital.spring.basics.sping_in_5_stages.basic.BinarySearchImpl;
import com.transdigital.spring.basics.sping_in_5_stages.scope.PersonDAO;

@Configuration
@ComponentScan
//@SpringBootApplication
public class SpingIn5StagesScopeApplication {

	//logger
	private static Logger LOGGER = LoggerFactory.getLogger(SpingIn5StagesScopeApplication.class);
	
	
	
	public static void main(String[] args) {
	
		
		
		
		
		
		
		//springapplicationcontext
		try(AnnotationConfigApplicationContext applicationcontext=new AnnotationConfigApplicationContext(SpingIn5StagesScopeApplication.class)){
		PersonDAO personDao=applicationcontext.getBean(PersonDAO.class);

		PersonDAO personDao2 = applicationcontext.getBean(PersonDAO.class);
		
		LOGGER.info("{}", personDao);
		LOGGER.info("{}", personDao.getJdbcConnection());
		
		
		LOGGER.info("{}", personDao2);
		LOGGER.info("{}", personDao.getJdbcConnection());	
		
		
		
		//outputfor both singleton beans
		//2021-11-18 10:56:16.323  INFO 19588 --- [           main] c.t.s.b.s.SpingIn5StagesScopeApplication : com.transdigital.spring.basics.sping_in_5_stages.scope.PersonDAO@20921b9b
		//2021-11-18 10:56:16.325  INFO 19588 --- [           main] c.t.s.b.s.SpingIn5StagesScopeApplication : com.transdigital.spring.basics.sping_in_5_stages.scope.JdbcConnection@55b5e331
		//2021-11-18 10:56:16.325  INFO 19588 --- [           main] c.t.s.b.s.SpingIn5StagesScopeApplication : com.transdigital.spring.basics.sping_in_5_stages.scope.PersonDAO@20921b9b
		//2021-11-18 10:56:16.325  INFO 19588 --- [           main] c.t.s.b.s.SpingIn5StagesScopeApplication : com.transdigital.spring.basics.sping_in_5_stages.scope.JdbcConnection@55b5e331
	
	
		//output is same even if you make jdbcconnectionclass autowired because personDAO is singleton
		//so how will you create multiple jdbc connections or different connections everytime 
		//so everytime we need different instance of jdbc connection for same personDAO we need "proxy"
		
		//in jdbc class add this
		//@Scope(value=ConfigurableBeanFactory.SCOPE_PROTOTYPE , proxyMode=ScopedProxyMode.TARGET_CLASS)
		}
	}

}
